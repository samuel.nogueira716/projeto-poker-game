var baralho = ["2C","3C","4C","5C","6C","7C","8C","9C","10C","JC","QC","KC","AC","2E","3E","4E","5E","6E","7E","8E","9E","10E","JE","QE","KE","AE","2O","3O","4O","5O","6O","7O","8O","9O","10O","JO","QO","KO","AO","2P","3P","4P","5P","6P","7P","8P","9P","10P","JP","QP","KP","AP"]
var copa = ["2C","3C","4C","5C","6C","7C","8C","9C","10C","JC","QC","KC","AC"]
var espada = ["2E","3E","4E","5E","6E","7E","8E","9E","10E","JE","QE","KE","AE"]
var ouro = ["2O","3O","4O","5O","6O","7O","8O","9O","10O","JO","QO","KO","AO"]
var paus = ["2P","3P","4P","5P","6P","7P","8P","9P","10P","JP","QP","KP","AP"]
var sorteados = []
var robo_1 = []
var robo_2 = []

function sortear(){
    robo_1 = []
    robo_2 = []
    sorteados = []
    for(i=0;i<5;i++){
        var sorteio = Math.floor(Math.random() * 51 + 1)
        sorteados.push(baralho[sorteio])
    }
    for(i=0;i<4;i++){
        var sorteio = Math.floor(Math.random() * 51 + 1)
        if(i<2){
            robo_1.push(baralho[sorteio])
        }else{
            robo_2.push(baralho[sorteio])
        }
    }
    document.querySelector(".visor_cartas_sorteadas").innerHTML = sorteados
    document.querySelector(".visor_robo_1").innerHTML = robo_1
    document.querySelector(".visor_robo_2").innerHTML = robo_2
    console.log(robo_1)
    console.log(robo_2)
    console.log(sorteados)
    document.querySelector(".respostas").innerHTML = ""
    renderizar_resultados("carta alta => ",carta_alta())
    renderizar_resultados("par => ",par())
    renderizar_resultados("dois par => ",dois_par())
    renderizar_resultados("tres cartas =>",tres_cartas())
    renderizar_resultados("flush => ",flush())
    renderizar_resultados("quadra => ",quadra())
    renderizar_resultados("royal flush =>",royal_flush())
}
function carta_alta(){
    //ROBO 1
    var mao_1 = robo_1.concat(sorteados)
    var cartas_altas_robo_1 = []
    mao_1.forEach(Element => {
        var posicoes = verificar_posicao(Element).sort(ordenar)
        cartas_altas_robo_1.push(posicoes[posicoes.length - 1])  
    })
    cartas_altas_robo_1.sort(ordenar)
    var carta_alta_robo_1 = cartas_altas_robo_1[cartas_altas_robo_1.length - 1]
     //ROBO 2
     var mao_2 = robo_2.concat(sorteados)
     var cartas_altas_robo_2 = []
     mao_2.forEach(Element => {
         var posicoes = verificar_posicao(Element).sort(ordenar)
         cartas_altas_robo_2.push(posicoes[posicoes.length - 1])  
     })
     cartas_altas_robo_2.sort(ordenar)
     var carta_alta_robo_2 = cartas_altas_robo_2[cartas_altas_robo_2.length - 1]
    
    if(carta_alta_robo_1>carta_alta_robo_2) return("robo_1") 
    if(carta_alta_robo_2>carta_alta_robo_1) return("robo_2")
    return ("empate")

}
function dois_par(){
    //ROBO 1
    var mao_1 = robo_1.concat(sorteados)
    var posicoes_cartas_robo_1 = []
    mao_1.forEach(Element => {
        posicoes_cartas_robo_1.push(verificar_posicao(Element))
    })
    var cartas_robo_1 = []
    posicoes_cartas_robo_1.forEach(Element => {
        cartas_robo_1.push([Element.find(carta => carta != -1),Element.indexOf(Element.find(carta => carta != -1))])
    })
    var contador_par_robo_1 = -7
    cartas_robo_1.forEach(Element => {
        for(i=0;i<7;i++){
            var carta = cartas_robo_1[i]
            if(carta[0] === Element[0] && carta[1] === Element[1]){
                contador_par_robo_1++
            }
        }
    })
    //ROBO 2
    var mao_2 = robo_2.concat(sorteados)
    var posicoes_cartas_robo_2 = []
    mao_2.forEach(Element => {
        posicoes_cartas_robo_2.push(verificar_posicao(Element))
    })
    var cartas_robo_2 = []
    posicoes_cartas_robo_2.forEach(Element => {
        cartas_robo_2.push([Element.find(carta => carta != -1),Element.indexOf(Element.find(carta => carta != -1))])
    })
    var contador_par_robo_2 = -7
    cartas_robo_2.forEach(Element => {
        for(i=0;i<7;i++){
            var carta = cartas_robo_2[i]
            if(carta[0] === Element[0] && carta[1] === Element[1]){
                contador_par_robo_2++
            }
        }
    })
    if(contador_par_robo_1 <3 && contador_par_robo_2 <3) return("sem par") 
    if(contador_par_robo_1>contador_par_robo_2) return("robo_1") 
    if(contador_par_robo_2>contador_par_robo_1) return("robo_2")
    
}
function tres_cartas(){
    var mao_1 = robo_1.concat(sorteados)
    var validador_trio_1 = false
    baralho.forEach(Element => {
        var ocorrencias = mao_1.filter(x => x === Element).length;
        if(ocorrencias>2) validador_trio_1 = true   
    })

    var mao_2 = robo_2.concat(sorteados)
    var validador_trio_2 = false
    baralho.forEach(Element => {
        var ocorrencias = mao_2.filter(x => x === Element).length;
        if(ocorrencias>2) validador_trio_2 = true   
    })

    if(validador_trio_1 == true) return("robo_1")
    if(validador_trio_2 == true) return("robo_2")
    return ("sem trio")
    
}
function flush(){
    //ROBO 1
    var mao_1 = robo_1.concat(sorteados)
    var mao_unida_1 = mao_1.join("")
    var naipes_1 = []
    for(i=0;i<mao_unida_1.length;i++){
        if(mao_unida_1[i] == "O" || mao_unida_1[i] == "C" || mao_unida_1[i] == "E" || mao_unida_1[i] == "P") naipes_1.push(mao_unida_1[i])
    }
    var conjunto_naipes_1 = naipes_1.sort().join("")
    var pesquisa_1 = [/OOOOO/g,/PPPPP/g,/CCCCC/g,/EEEEE/g]
    var validador_flush_1 = false
    pesquisa_1.forEach(Element => {
        if(conjunto_naipes_1.search(Element) != -1 ) validador_flush_1 = true
    })
    //ROBO 2
    var mao_2 = robo_2.concat(sorteados)
    var mao_unida_2 = mao_2.join("")
    var naipes_2 = []
    for(i=0;i<mao_unida_2.length;i++){
        if(mao_unida_2[i] == "O" || mao_unida_2[i] == "C" || mao_unida_2[i] == "E" || mao_unida_2[i] == "P") naipes_2.push(mao_unida_2[i])
    }
    var conjunto_naipes_2 = naipes_2.sort().join("")
    var pesquisa_2 = [/OOOOO/g,/PPPPP/g,/CCCCC/g,/EEEEE/g]
    var validador_flush_2 = false
    pesquisa_2.forEach(Element => {
        if(conjunto_naipes_2.search(Element) != -1 ) validador_flush_2 = true
    })
    if(validador_flush_1 == true) return("robo_1")
    if(validador_flush_2 == true) return("robo_2")
    return ("sem flush")
    
}
function royal_flush(){
    var mao_1 = robo_1.concat(sorteados)
    var posicoes_baralho_1 = []
    var posicoes_royal_flush = [/89101112/g,/2122232425/g,/34,35,36,37,38/g,/4748495051/g]
    mao_1.forEach(Element => {
        posicoes_baralho_1.push(baralho.indexOf(Element))
    })
    var posicoes_cartas_1 = posicoes_baralho_1.sort(ordenar).join("")
    var validador_royal_flush_1 = false
    posicoes_royal_flush.forEach(Element => {
        if(posicoes_cartas_1.search(Element) != -1) validador_royal_flush_1 = true
    })

    var mao_2 = robo_2.concat(sorteados)
    var posicoes_baralho_2 = []
    mao_2.forEach(Element => {
        posicoes_baralho_2.push(baralho.indexOf(Element))
    })
    var posicoes_cartas_2 = posicoes_baralho_2.sort(ordenar).join("")
    var validador_royal_flush_2 = false
    posicoes_royal_flush.forEach(Element => {
        if(posicoes_cartas_2.search(Element) != -1) validador_royal_flush_2 = true
    })
    if(validador_royal_flush_1 == true) return("robo_1")
    if(validador_royal_flush_2 == true) return("robo_2")
    return ("sem flush")
}
function quadra(){
    var mao_1 = robo_1.concat(sorteados)
    var validador_quadra_1 = false
    baralho.forEach(Element => {
        var ocorrencias = mao_1.filter(x => x === Element).length;
        if(ocorrencias>3) validador_quadra_1 = true   
    })

    var mao_2 = robo_2.concat(sorteados)
    var validador_quadra_2 = false
    baralho.forEach(Element => {
        var ocorrencias = mao_2.filter(x => x === Element).length;
        if(ocorrencias>3) validador_quadra_2 = true   
    })

    if(validador_quadra_1 == true) return("robo_1")
    if(validador_quadra_2 == true) return("robo_2")
    return ("sem quadra")
}
function par(){
    var mao_1 = robo_1.concat(sorteados)
    var validador_par_1 = 0
    baralho.forEach(Element => {
        var ocorrencias = mao_1.filter(x => x === Element).length;
        if(ocorrencias%2 == 0) validador_par_1++
    })
    var mao_2 = robo_2.concat(sorteados)
    var validador_par_2 = 0
    baralho.forEach(Element => {
        var ocorrencias = mao_2.filter(x => x === Element).length;
        if(ocorrencias%2 == 0) validador_par_2++
    })
    if(validador_par_1 > validador_par_2) return("robo_1")
    if(validador_par_2 > validador_par_1) return("robo_2")
    if(validador_par_2 === validador_par_1) return("empate")
    return ("sem par")
}
function verificar_posicao(carta){
    var posicoes = []
    posicoes.push(copa.indexOf(carta))
    posicoes.push(espada.indexOf(carta))
    posicoes.push(ouro.indexOf(carta))
    posicoes.push(paus.indexOf(carta))
    return posicoes
}
function ordenar(a,b){
    if(a>b) return 1
    if(a<b) return -1
    return 0
}
function renderizar_resultados(texto,resposta){
    document.querySelector(".respostas").innerHTML += texto+resposta+"<br>"
}