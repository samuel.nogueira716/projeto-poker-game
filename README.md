# Projeto poker game
    Projeto criado com base na seguinte situação
    Uma tripulação de astronautas está jogando poker durante uma missão espacial. Entediados, eles decidiram colocar dois  robôs para competirem.
    Supondo que você seja um dos astronautas, crie um algoritmo que sorteie 5 cartas aleatórias para ficarem na mesa e mais 2 cartas para cada robô.
    Os robôs devem juntar as cartas que possuem na mão com as cartas da mesa para formar uma combinação de poker. O algoritmo criado deve identificar quais combinações foram formadas, decidindo o vencedor ou declarando empate, seguindo a hierarquia mostrada na tabela abaixo. Exemplo: Se o robô 1 tiver um “Full House” e o robô 2 tiver um “Trio”, o robô 1 vence.

## TECNOLOGIAS
-HTML
-CSS
-JAVASCRIPT
